# Experis Academy Hibernate

Group project for Noroff's Java Accelerate course to learn hibernate. The task is to create a datastore to store movies, their characters and which franchise it belongs to. We have in this project created an API with hibernate that provides endpoints with full CRUD to manipulate characters, movies and franchises.

## CRUD

### Documentation
See in "Documentation" directory for postman api documentation and JavaDoc.

### Reports
There are three different reports you can get.
* Get all movies in a franchise
* Get all characters in a movie
* Get all characters in a franchise

### Notes
We assume that characters or movies cannot stretch across different franchises,
as they are often in different universes. Because of this, we have
chosen these rules for the deletion of records:
- Characters = Row deleted in character table and references removed in
other tables
- Movies = Row deleted in movie table and references removed in
other tables. If a character has no more movies after movie deletion,
the character row will be removed as well.
- Franchise = Row deleted in franchise table as well as all other
associations in movies and character table.


## Contributers
* Jan Zimmer - https://gitlab.com/janzim
* Jostein Olstad - https://gitlab.com/Josteiol
* Andreas Mikalsen - https://gitlab.com/andreasmi