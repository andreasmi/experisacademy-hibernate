package com.experis.andreas.JavaHibernate.repositories;

import com.experis.andreas.JavaHibernate.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Long> {
}
