package com.experis.andreas.JavaHibernate.repositories;

import com.experis.andreas.JavaHibernate.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Long> {
}
