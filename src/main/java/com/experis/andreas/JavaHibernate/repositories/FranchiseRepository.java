package com.experis.andreas.JavaHibernate.repositories;

import com.experis.andreas.JavaHibernate.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
