package com.experis.andreas.JavaHibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
/*@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")*/
public class Character {
    @Id
    @GeneratedValue
    private long id;

    private String firstName;
    private String lastName;
    private String alias;

    private String gender;

    private String picture;

    @ManyToMany(mappedBy = "characters")
    public List<Movie> movies;

    @JsonGetter("movies")
    public List<String> movies() {
        return movies.stream()
                .map(movie -> {
                    return "/api/v1/movies/" + movie.getId();
                }).collect(Collectors.toList());
    }

    //Constructor

    public Character(long id, String firstName, String lastName, String alias, String gender, String picture) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movies = movies;
    }

    public Character() {
    }


    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
