package com.experis.andreas.JavaHibernate.controllers;

import com.experis.andreas.JavaHibernate.models.Character;
import com.experis.andreas.JavaHibernate.models.Franchise;
import com.experis.andreas.JavaHibernate.models.Movie;
import com.experis.andreas.JavaHibernate.repositories.CharacterRepository;
import com.experis.andreas.JavaHibernate.repositories.FranchiseRepository;
import com.experis.andreas.JavaHibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Gets all franchises
     * @return List of franchises
     */
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises,status);
    }

    /**
     * Get a specific franchise
     * @param id The franchise is
     * @return The franchise
     */
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        HttpStatus status;
        Franchise franchise = new Franchise();
        if(!franchiseRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(franchise, status);
        }

        franchise = franchiseRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(franchise,status);
    }


    /**
     * Post a new franchise
     * @param franchise The franchise to post
     * @return The posted franchise
     */
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        Franchise returnFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Put a franchise
     * @param id The franchise id
     * @param franchise The new franchise
     * @return The new updated franchise
     */
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if(!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise,status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Delete the franchise from the database and from all movies and characters that
     * played in it
     * @param id unique id of franchise
     * @return the deleted franchise
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Franchise> removeFranchise(@PathVariable Long id){
        HttpStatus status;
        Franchise franchise = new Franchise();
        if(!franchiseRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(franchise, status);
        }

        franchise = franchiseRepository.findById(id).get();

        for(int i = 0; i < franchise.movies.size(); i++){
            Movie movie = franchise.movies.get(i);

            // remove all movies and all characters
            // when it doesn't belong to the franchise anymore
            // for each character in movie
            for(int j = 0; j < franchise.movies.size(); j++){
                Character character = movie.characters.get(j);
                // remove movie and character relationships
                character.movies.clear();
                movie.characters.remove(j);
                characterRepository.deleteById(character.getId());
            }
            movieRepository.deleteById(movie.getId());

            franchise.movies.remove(i);
        }

        franchiseRepository.deleteById(id);

        status = HttpStatus.OK;
        return new ResponseEntity<>(franchise,status);
    }
    /**
     * Gets all movies in a franchise
     * @param id The franchise id
     * @return List of movies
     */
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> getMoviesInFranchise(@PathVariable Long id){
        HttpStatus status;

        if(!franchiseRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        Franchise franchise = franchiseRepository.findById(id).get();

        status = HttpStatus.OK;
        return new ResponseEntity<>(franchise.getMovies(), status);
    }

    /**
     * Gets all characters in a franchise
     * @param id The franchise is
     * @return List of characters
     */
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getCharactersInFranchise(@PathVariable Long id){
        HttpStatus status;

        if(!franchiseRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        List<Movie> moviesInFranchise = franchiseRepository.findById(id).get().getMovies();
        HashMap<Long, Character> charactersMap = new HashMap<>();

        moviesInFranchise.stream().map(Movie::getCharacters)
                .forEach(list -> {
                    list.forEach(character -> {
                        charactersMap.put(character.getId(), character);
                    });
                });

        List<Character> characters = new ArrayList<>(charactersMap.values());


        status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }
}
