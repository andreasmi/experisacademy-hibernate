package com.experis.andreas.JavaHibernate.controllers;

import com.experis.andreas.JavaHibernate.models.Character;
import com.experis.andreas.JavaHibernate.models.Movie;
import com.experis.andreas.JavaHibernate.repositories.CharacterRepository;
import com.experis.andreas.JavaHibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Gets all the movies
     * @return List of movies
     */
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movies = movieRepository.findAll();
        for(int i = 0; i < movies.size(); i++){
            System.out.println(movies.get(i).getTitle());
        }
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies,status);
    }

    /**
     * Gets a specific movie
     * @param id The movie id
     * @return The movie
     */
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id){
        HttpStatus status;
        Movie movie = new Movie();
        if(!movieRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(movie, status);
        }

        movie = movieRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(movie,status);
    }

    /**
     * Post a new movie
     * @param movie The movie to post
     * @return The posted movie
     */
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        Movie returnMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Puts a movie
     * @param id The movie id
     * @param movie The new movie object
     * @return The newly updated movie
     */
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        Movie returnMovie = new Movie();
        HttpStatus status;

        if(!id.equals(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie,status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Gets all characters in a movie
     * @param id The movie id
     * @return List of characters
     */
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getCharactersInMovie(@PathVariable Long id){
        HttpStatus status;

        if(!movieRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        Movie movie = movieRepository.findById(id).get();

        status = HttpStatus.OK;
        return new ResponseEntity<>(movie.getCharacters(), status);
    }

    /**
     * Delete the movie from the database and from all characters that
     * played in it
     * @param id unique id of movie
     * @return the deleted movie
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Movie> removeMovie(@PathVariable Long id){
        HttpStatus status;
        Movie movie = new Movie();
        if(!movieRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(movie, status);
        }

        movie = movieRepository.findById(id).get();

        for(int i = 0; i < movie.characters.size(); i++){
            Character character = movie.characters.get(i);
            character.movies.remove(movie);

            // we want to remove character when it doesn't
            // belong to any movies anymore
            if(character.movies.size() == 0){
                character = movie.characters.remove(i);
                characterRepository.deleteById(character.getId());
            }
        }

        movieRepository.deleteById(id);

        status = HttpStatus.OK;
        return new ResponseEntity<>(movie,status);
    }
}
