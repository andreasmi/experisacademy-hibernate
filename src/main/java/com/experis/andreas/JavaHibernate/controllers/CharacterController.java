package com.experis.andreas.JavaHibernate.controllers;

import com.experis.andreas.JavaHibernate.models.Character;
import com.experis.andreas.JavaHibernate.repositories.CharacterRepository;
import com.experis.andreas.JavaHibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/characters")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Gets all the characters
     * @return List of characters
     */
    @GetMapping()
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters,status);
    }

    /**
     * Get a specific character
     * @param id The character is
     * @return The character
     */
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacter(@PathVariable Long id){
        HttpStatus status;
        Character character = new Character();
        if(!characterRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(character, status);
        }

        character = characterRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(character,status);
    }

    /**
     * Add a new character
     * @param character the character that was added
     * @return the character that was created
     */
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        Character returnCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Put a character
     * @param id The character id
     * @param character The new character object
     * @return The new updated character
     */
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        Character returnCharacter = new Character();
        HttpStatus status;

        if(!id.equals(character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter,status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Delete the character from the database and from all movies it played in
     * @param id unique id of character
     * @return the deleted character
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Character> removeCharacter(@PathVariable Long id){
        HttpStatus status;
        Character character = new Character();
        if(!characterRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(character, status);
        }

        character = characterRepository.findById(id).get();

        for(int i = 0; i < character.movies.size(); i++){
            character.movies.get(i).characters.remove(character);
        }

        characterRepository.deleteById(id);

        status = HttpStatus.OK;
        return new ResponseEntity<>(character,status);
    }
}
